package com.example;

import java.net.InetAddress;

import javax.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.health.Health;
import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.jboss.dmr.ModelNode;
import org.jboss.logging.Logger;
import org.jboss.as.controller.client.ModelControllerClient;

@Health
@ApplicationScoped
public class HealthChecks implements HealthCheck {
    private static Logger logger = Logger.getLogger(HealthChecks.class);
    @Override
    public HealthCheckResponse call() {
       
        		if (!ApplicationConfig.IS_ALIVE.get()) {
            logger.error("I already dead");
            return HealthCheckResponse.named("server-state").down().build();
			
		} else {
			logger.info("I'm alive");
			return HealthCheckResponse.named("server-state").up().build();
		}
        // ModelNode op = new ModelNode();
        // op.get("address").setEmptyList();
        // op.get("operation").set("read-attribute");
        // op.get("name").set("suspend-state");

        // try (ModelControllerClient client = ModelControllerClient.Factory.create(
        //         InetAddress.getByName("localhost"), 8080)) {
        //     ModelNode response = client.execute(op);

        //     if (response.has("failure-description")) {
        //         throw new Exception(response.get("failure-description").asString());
        //     }

        //     boolean isRunning = response.get("result").asString().equals("RUNNING");
        //     if (isRunning) {
        //         return HealthCheckResponse.named("server-state").up().build();
        //     } else {
        //         return HealthCheckResponse.named("server-state").down().build();
        //     }
        // } catch (Exception e) {
        //     throw new RuntimeException(e);
        // }
        // return null;
	}

    
}