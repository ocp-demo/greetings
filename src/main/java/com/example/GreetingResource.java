package com.example;
import org.jboss.logging.Logger;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/")
public class GreetingResource {

	private static final String template = "Hello, %s!";
	private static Logger logger = Logger.getLogger(GreetingResource.class);
	@GET
	@Path("/greeting")
	@Produces("application/json")
	public Greeting greeting(@QueryParam("name") String name) {
		logger.info("Request for " + name);
		if (!ApplicationConfig.IS_ALIVE.get()) {
			throw new WebApplicationException(Response.Status.SERVICE_UNAVAILABLE);
		}
		String suffix = name != null ? name : "World";
		return new Greeting(String.format(template, suffix),getLocalHostname());
	}

	@GET
	@Path("/stop")
	public Response stop() {
		logger.info("It's time to die");
		if (!ApplicationConfig.IS_ALIVE.get()) {
			throw new WebApplicationException(Response.Status.SERVICE_UNAVAILABLE);
		}

		ApplicationConfig.IS_ALIVE.set(false);
		return Response.ok("killed").build();

	}

	@GET
	@Path("/start")
	public Response start() {

		if (!ApplicationConfig.IS_ALIVE.get()) {
			logger.debug("It's time to resurrect");
			ApplicationConfig.IS_ALIVE.set(true);
		} else {
			logger.debug("Still alive. no need to do anything");
		}
		ApplicationConfig.IS_ALIVE.set(true);
		return Response.ok().build();

	}
	private String getLocalHostname() {
		InetAddress inetAddr;
		String hostname = "";
		try {
			inetAddr = InetAddress.getLocalHost();
			hostname = inetAddr.getHostName();
		} catch (UnknownHostException e) {
			logger.error("Error get local hostname");
			logger.error(e.getMessage());
			e.printStackTrace();
		}
		return hostname;		


	}
}
