package com.example;

import static org.junit.Assert.assertTrue;
import com.example.Greeting;

import org.junit.Ignore;
import org.junit.Test;

public class GreetingTest {

    @Test
	public void testNewGreeting() {
        Greeting greeting = new Greeting("Jack","pod-1-xyz");
        assertTrue("Jack".equals(greeting.getContent()));
        assertTrue("pod-1-xyz".equals(greeting.getHostname()));     
	}
    @Test
    @Ignore
    public void testNewGreetingFailed(){
        Greeting greeting = new Greeting("John","pod-1-xxz");
        assertTrue("James".equals(greeting.getContent()));
        assertTrue("pod-1-xxxxxz".equals(greeting.getHostname()));  
    }
    
}